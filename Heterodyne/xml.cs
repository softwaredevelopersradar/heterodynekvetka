﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Heterodyne
{
    public class xmlFile
    {
        private string path;

        public xmlFile(string path)
        {
            this.path = path;
        }

        public void XmlWriteValue(string Key, string Value)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlElement xnode in xRoot)
            {
                XmlNode attr = xnode.Attributes.GetNamedItem("name");
                if (attr.InnerText == Key)
                {
                    xnode.ChildNodes[0].InnerText = Value;
                    xDoc.Save(path);
                }
            }
        }

        public string XmlReadValue(string Key)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(path);
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlElement xnode in xRoot)
            {
                XmlNode attr = xnode.Attributes.GetNamedItem("name");

                if (attr.InnerText == Key)
                {
                    return xnode.ChildNodes[0].InnerText;
                }
            }
            return "";
        }
    }
}
